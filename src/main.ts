import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import App from './App.vue';
import { register } from '../form-builder/components/registerComponents';

Vue.config.productionTip = false;

Vue.use(VueAxios, axios);

// Register components
register();

new Vue({
  render: (h) => h(App),
}).$mount('#app');
