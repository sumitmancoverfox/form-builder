import { IFormField, IForm, IFormSection } from 'form-builder/shared/interfaces';

export interface IFormCustom extends IForm {
	formSections: IFormSectionCustom[];
}
export interface IFormSectionCustom extends IFormSection {
	sectionFields: IFormFieldCustom[];
}
export interface IFormFieldCustom extends IFormField {
	fieldValue: any;
	label: string;
	options: {[key: string]: any};
}
