# Form Builder

Form builder is created with the intent to make the use of dynamic multi-step forms easier. It enables one to use any custom/third party input components and embeds it in the dynamic form builder.

Please find the documentation links here:

[API documentation](https://sumitmancoverfox.gitlab.io/form-builder/docs/api/)

[Component documentation](https://sumitmancoverfox.gitlab.io/form-builder/docs/component/#/components/FormBuilder)

### Installation
```npm install --save @sumitman/vue-form-builder```
<small>Note: Vue 2.6.10 and higher required.<br/>Form builder also requires node-sass and sass-loader as dev dependencies as it uses scss. If the application does not already use scss use `npm install --save-dev node-sass sass-loader`.</small>

Add form-builder style sheet to your project. eg: If your project uses scss, in App.vue
```SCSS
<style>
@import '@sumitman/vue-form-builder/form-builder/assets/style.scss';
</style>
```
One may also import it in typescript
```typescript
import '@sumitman/vue-form-builder/form-builder/assets/style.scss';
```

### Basic usage
1. Register default input components globally, by calling register function in the main.ts file.
```typescript
import { registerFormComponents } from '@sumitman/vue-form-builder/form-builder';

registerFormComponents();
```
2. Import form builder component
```typescript
import { FormBuilder } from '@sumitman/vue-form-builder/form-builder';
```
3. Register form-builder in your component.
4. Add `<form-builder/>` to template and set an `:id` to the form builder.
```html
<form-builder
	v-model="activeFormId"
	:id="FORM_BUILDER_ID"
	:forms="forms"
	:data="data"
	@submit="onSubmit"
	@stepClick="onStepClick($event);"
	@previousFormClick="onNavigate($event);"
/>
```
5. Create `forms` schema, of the type `IForm[]`
eg:
```json
[
	{
		"formId": "form1",
		"formName": "Personal",
		"formTitle": "Personal Details",
		"formDescription": "Tell us about yourself...",
		"formCTA": "Submit and next",
		"formIsActive": false,
		"formSections": [
			{
				"sectionId": "sectionBasic",
				"sectionHeading": "Basic",
				"sectionFields": [
					{
						"fieldId": "fullName",
						// fieldType value uses enum `InputControlTypes` by default
						// refer api documentation
						"fieldType": "text",
						"fieldHidden": false,
						"placeholder": "Enter full name...",
						"label": "Full Name"
					}
				]
			}
		]
	}
]
```
6. Let `data` be an empty object.
Note: `data` is a dictionary of fieldId as key and form values. This can also be used to pre-fill values. This object is managed by `form-builder`.
1. Let `v-model` variable `activeFormId` be an empty string or form id of the first form. If left empty, and will be initialized by the form-builder.
1. Set callback function for `@submit` event
1. Set callback functions for `@stepClick` and `@previousFormClick` events.

Note: Changing the active form (v-model) is a manual process and is to be set in the events mentioned above.
This allows multiple ways of implementing the form-builder. eg. Opening every form on a new route or simply change the active form on the same page. Changing the active form can also be controlled according to form validations if any.

Note: One can also use form-builder with steps as accordions by importing:
```typescript
import { FormBuilderAccordion } from '@sumitman/vue-form-builder/form-builder';
```
`form-builder-accordion` has similar props and events as `form-builder`

### Binding events to form fields
Event functions can be bound to form fields using decorators `@formEvent`, `@formEventOfType` and `@formEventForAll`.
These can be found in `@sumitman/vue-form-builder/form-builder`
1. Define a function for onInput event
```typescript
private onInput(formEventArgs: IFormEventArgs, value: string): void {
	// log user input
	log(formEventArgs.field.label, value);
}
```
The first parameter of the function should be `formEventArgs`, which receives `field` instance of the field type and `data`, an optional parameter which can be passed. The function can then expect other data parameters sent by the event.
2. Add decorator to the function
```typescript
@formEvent('formBuilderId', ['fieldName1', 'fieldName2'], 'input')
private onInput(formEventArgs: IFormEventArgs, value: string): void {
	// log user input
	log(formEventArgs.field.label, value);
}
```
It accepts form-builder id, list of field ids, event name and optional data as parameters.

### Using custom/third party input components
form-builder allows one to use custom or third party input components. By default it uses a set of type name and component map `TYPE_MAP`, found in `@sumitman/vue-form-builder/form-builder`, which is set to `typeMap` prop of the form-builder. This prop can be overridden to include your own input components and hence can be used with any vue input widget library.

<small>Note: All input components to be used with form-builder must work on v-model</small>

<small>Note: The default input components are very basic and minimilistic as the major idea of this library is to let one use third party components. Nevertheless, future releases will improve the quality of these components as well.</small>

1. Register input component globally, so that form-builder can use them.
1. Create type classes for the input components. The classes should implement IFormFieldType interface. Props required to be passed to the component can also be added to these classes (except v-model). Constructor for this class should accept object which initializes the props.
```typescript
import { IFormFieldType } from '@sumitman/vue-form-builder/form-builder';

export class InputTextType implements IFormFieldType {
	public componentName: string = 'input-text';
	public events: {[key: string]: Function} = {};
	public placeholder: string;
	public label: string;
	public constructor(data: any) {
		this.placeholder = data.placeholder;
		this.label = data.label;
	}
}
```
3. Create a type map so that fieldType can associate a type to the component.
```typescript
import { IFormFieldTypeConstructor } from '@sumitman/vue-form-builder/form-builder';
private typeMapping: { [key: string]: IFormFieldTypeConstructor } = {
	text: InputTextType,
};
```
4. Send typeMapping to `:typeMap` prop of form-builder.
```html
<form-builder
	v-model="activeFormId"
	:id="FORM_BUILDER_ID"
	:forms="forms"
	:data="data"
	:typeMap="typeMapping"
	@submit="onSubmit"
	@stepClick="onStepClick($event);"
	@previousFormClick="onNavigate($event);"
/>
```
5. `:forms` schema can now have input component props as properties on IFormField. Note: IFormField can be extended for typescript to include new properties.

Note: If one needs to add new input types and keep the defaults or modify some of the default types, `TYPE_MAP` can spread in the `typeMapping` variable.

### Form Review
This package also includes a `form-review` component which is like a summary page for the form set. It also provides edit events such that the concerned form can be active in the form-builder.
1. import and register `form-review` component
```typescript
import { FormReview } from '@sumitman/vue-form-builder/form-builder';
```
2. Add `<form-review/>` to the template
1. Pass `:id`, `:data` and `:forms` similar to the one like `form-builder`.

#### Formatters
`vue-form-builder` provides ways to format data displayed for each field in the `form-review` component.
Following is a list of ways one can format the field data display, with increasing order of precedence:
1. Set `fieldReviewFormat` field of `IFormField` as a string and add `$$value$$` for where value of the field needs to be replaced. eg: `fieldReviewFormat: "Full Name: $$value$$"` renders to `Full Name: abc`
1. Set a formatter function which should return the string to be displayed. The formatter function will receive field object to extract the value to be formatted.
This can be done using `@reviewFieldFormatter`, `@reviewFieldFormatterOfType` and `@reviewFieldFormatterForAll` decorators (check api documentation for further explanation).
1. Use slots provided by `form-review`, where it receives field object as slot properties. List of slots and options can be found in component documentation.

The same can also be done for section headers and form headers with `@reviewSectionFormatter`, `@reviewSectionFormatterForAll`, `@reviewFormFormatter` and `@reviewFormFormatterForAll`
