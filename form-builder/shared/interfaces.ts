/**
 * To be implemented by custom field component classes
 */
export interface IFormFieldType {
	/** Name of the component */
	componentName: string;
	/** Key-value pairs of event name and related function. <br/>*Note: Use bind for functions with arguments* */
	// tslint:disable-next-line
	events: { [key: string]: Function };
}
/** Type alias for [[IFormFieldType]] */
export type IFormFieldTypeConstructor = new (data: any) => IFormFieldType;

/**
 * Form object
 */
export interface IForm {
	/** Unique identifier and a `required` property. */
	formId: string;
	/** Name of the form. This appears as the step name by default. */
	formName: string;
	/** Title of the form. This appears on the form as the title. */
	formTitle?: string;
	/** String to give the form more description. Appears as a tip below the title. */
	formDescription?: string;
	/** Text to appear on the form's submit button. */
	formCTA: string;
	/**
	 * Function to let form know when to disable/enable the submit button.
	 * The function should accept [[IForm]] as input and returns `:boolean` true to disable the button.
	 */
	formCTAIsDisabled?: (form: IForm) => boolean;
	/** Sets form to be active. */
	formIsActive: boolean;
	/** Forms have sections and sections will have form fields */
	formSections: IFormSection[];
	/** Indicates if the form is submitted or not */
	formIsSubmitted: boolean;
	/** Hides the form */
	formHidden: boolean;
	/** Excludes form and its fields from the review page */
	formExcludeFromReview?: boolean;
	/**
	 * Array of section ids to be included for review page.
	 * The order of section ids in this array will reflected on the review page.
	 * If this property is left undefined, all sections are included in the
	 * review page in the order they appear on the form when rendered.
	 */
	formReviewSectionIds?: string[];
	/** Function to format the Title heading.
	 * The function should accept [[IForm]] and [[IForm]][] as inputs and return the formatted `:string`.
	 */
	formReviewFormatter?: (
		form: IForm,
		forms: IForm[],
	) => string;
	/** String format to display [[formName]]. Use `$$value$$` as place holder for [[formName]]
	 * <br/>*Note: [[formReviewFormat]] has lesser preference than [[formReviewFormatter]].*
	 */
	formReviewFormat?: string;
}
/**
 * Forms [[IForm]] contain 1 or more sections
 */
export interface IFormSection {
	/** Unique identifier and a `required` property */
	sectionId: string;
	/** Title of the section. Appears on the section header. */
	sectionHeading: string;
	/** String to give the section more description. Appears as a tip below the title. */
	sectionDescription?: string;
	/** Array of fields, which will be rendered on the form */
	sectionFields: IFormField[];
	/** Hides the complete section */
	sectionHidden: boolean;
	/** Excludes section and its fields from the review page */
	sectionExcludeFromReview?: boolean;
	/** Array of field ids to be included for review page.
	 * The order of field ids in this array will be reflected on the review page.
	 * If this property is left undefined, all fields are included in the review page in the order they appear on the form.
	 */
	sectionReviewFieldIds?: string[];
	/** Function to format the section heading.
	 * The function should accept [[IFormSection]], [[IForm]] and [[IForm]][] as inputs and return the formatted `:string`.
	 */
	sectionReviewFormatter?: (
		section: IFormSection,
		form: IForm,
		forms: IForm[],
	) => string;
	/** String format to display [[sectionHeading]]. Use `$$value$$` as place holder for [[sectionHeading]]
	 * <br/>*Note: [[sectionReviewFormat]] has lesser preference than [[sectionReviewFormatter]].*
	 */
	sectionReviewFormat?: string;
}
/**
 * Sections [[IFormSection]] contain 1 or more form fields
 */
export interface IFormField {
	/** Unique identifier and a `required` property */
	fieldId: string;
	/** User defined field type name. eg: text, radio, etc. */
	fieldType: any;
	/** Hides the field */
	fieldHidden: boolean;
	/** Class object which initializes props and events required by the custom type component */
	fieldObject?: IFormFieldType;
	/** Is set invalid when type component map does not exist */
	fieldIsInvalid?: boolean;
	/** Excludes field from appearing on the review page. */
	fieldExcludeFromReview?: boolean;
	/** Function to format the field value.
	 * The function should accept [[IFormField]], [[IFormSection]], [[IForm]] and [[IForm]][]
	 *  as inputs and return formatted `:string`.
	 */
	fieldReviewFormatter?: (
		value: any,
		field: IFormField,
		section: IFormSection,
		form: IForm,
		forms: IForm[],
	) => string;
	/** String form to display form data. Use $$value$$ as placeholder for data.
	 * <br/>*Note: [[fieldReviewFormat]] has lesser preference than [[fieldReviewFormatter]].*
	 */
	fieldReviewFormat?: string;
}
export interface INavigateClickEvent {
	/** Unique identifier form */
	formId: string;
	/** index position of the form within form array */
	formIndex: number;
}
export interface IStepClickEvent extends INavigateClickEvent {
	/** Indicates if the form can be navigated or not. */
	canNavigate: boolean;
}
/**
 * Action object to store parameters to store and run functions
 */
export interface IFormAction {
	/** Stores function action */
	// tslint:disable-next-line
	fn: Function;
	/** Object to be sent to the function */
	args: any;
}
/**
 * Stores event-element mapping w.r.t fields, types or aplicable to all
 */
export interface IFormEventMapSchema {
	/** Store dictionary of field Id as key and dictionary of event name as key and related event action */
	fields: {
		/** field id */
		[fieldId: string]: {
			/** event name for field id */
			[event: string]: IFormAction,
		},
	};
	/**
	 * Store dictionary of type name as key and dictionary of event name as key
	 * and related event action map to multiple fields
	 */
	types: {
		/** type name */
		[type: string]: {
			/** event name for field of type name */
			[event: string]: IFormActionMultiple,
		},
	};
	/** Store dictionary of event name as key and related event action map to multiple fields */
	all: {
		/** event name */
		[event: string]: IFormActionMultiple,
	};
}
/**
 * Used to map an action to multiple fields excluding a few
 */
export interface IFormActionMultiple {
	/** List of unique identifiers to be excluded from mapping actions to */
	excludedIds?: string[];
	/** action to be associated to multiple elements */
	action: IFormAction;
}
/**
 * Stores formatter function-element mapping w.r.t fields, sections, types and ones applicable to all
 */
export interface IFormFormatterMapSchema {
	/** Store dictionary of field id as key and related formatter action */
	fields: {
		/** field id */
		[fieldId: string]: IFormAction,
	};
	/** Store dictionary of section id as key and related formatter action */
	sections: {
		/** section id */
		[sectionId: string]: IFormAction,
	};
	/** Store dictionary of form id as key and related formatter action */
	forms: {
		/** form id */
		[formId: string]: IFormAction,
	};
	/** Store dictionary of type name as key and related formatter action map to multiple fields */
	types: {
		/** type name */
		[type: string]: IFormActionMultiple,
	};
	/** Store formatter action map to multiple fields */
	all?: IFormActionMultiple;
}

/**
 * First object received by form events
 */
export interface IFormEventArgs {
	/** Form field to which the event is bound */
	field: IFormField;
	/** Any data passed by the [[formEvent]] decorators */
	data: any;
}
