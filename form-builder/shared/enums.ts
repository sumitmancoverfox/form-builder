/**
 * Mode in which `Accordion` component operates
 */
export enum AccordionMode {
	/**
	 * Any collapsible is accessible
	 */
	EnableAll,
	/**
	 * Collapsibles which are submitted or are next in line to be submitted, are accessible
	 */
	DisableUntilSubmit,
	/**
	 * Collapsibles before the currently active collapsible, which are also submitted, are accessible
	 */
	DisableFromOpen,
}
