/**
 * Default Types used in form
 */
export enum InputControlTypes {
	/** String text input */
	Text = 'text',
	/** Radio button group */
	Radio = 'radio',
	/** Dropdown input */
	Dropdown = 'dropdown',
	/** Checkbox button group */
	Checkbox = 'checkbox',
	/** Multiline string input */
	Textarea = 'textarea',
}
