export const INPUT_TEXT: string = 'vfb-input-text';
export const INPUT_RADIO: string = 'vfb-input-radio';
export const DROP_DOWN: string = 'vfb-drop-down';
export const INPUT_CHECKBOX: string = 'vfb-input-checkbox';
export const INPUT_TEXTAREA: string = 'vfb-input-textarea';
