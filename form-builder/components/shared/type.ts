import { IFormFieldType } from '../../shared/interfaces';
import { INPUT_TEXT, INPUT_RADIO, DROP_DOWN, INPUT_CHECKBOX, INPUT_TEXTAREA } from './constants';

/** Type class for InputType component */
export class InputTextType implements IFormFieldType {
	/** InputType component name */
	public componentName: string = INPUT_TEXT;
	/** Event dictionary with event name as key and callback function as value */
	// tslint:disable-next-line
	public events: {[key: string]: Function} = {};
	/** InputType placeholder prop */
	public placeholder: string;
	/** InputType label prop */
	public label: string;
	/** Initializes InputType props */
	public constructor(data: any) {
		this.placeholder = data.placeholder;
		this.label = data.label;
	}
}

/** Type class for InputRadio component */
export class InputRadioType implements IFormFieldType {
	/** InputRadio component name */
	public componentName: string = INPUT_RADIO;
	/** Event dictionary with event name as key and callback function as value */
	// tslint:disable-next-line
	public events: {[key: string]: Function} = {};
	/** InputRadio options prop */
	public options: {[key: string]: string};
	/** InputRadio label prop */
	public label: string;
	/** Initializes InputRadio props */
	public constructor(data: any) {
		this.options = data.options;
		this.label = data.label;
	}
}

/** Type class for DropDown component */
export class DropDownType implements IFormFieldType {
	/** DropDown component name */
	public componentName: string = DROP_DOWN;
	/** Event dictionary with event name as key and callback function as value */
	// tslint:disable-next-line
	public events: {[key: string]: Function} = {};
	/** DropDown options prop */
	public options: {[key: string]: string};
	/** DropDown label prop */
	public label: string;
	/** DropDown placeholder prop */
	public placeholder: string;
	/** Initializes DropDown props */
	public constructor(data: any) {
		this.options = data.options;
		this.label = data.label;
		this.placeholder = data.placeholder;
	}
}

/** Type class for InputCheckbox component */
export class InputCheckboxType implements IFormFieldType {
	/** InputCheckbox component name */
	public componentName: string = INPUT_CHECKBOX;
	/** Event dictionary with event name as key and callback function as value */
	// tslint:disable-next-line
	public events: {[key: string]: Function} = {};
	/** InputCheckbox options prop */
	public options: {[key: string]: string};
	/** InputCheckbox label prop */
	public label: string;
	/** Initializes InputCheckbox props */
	public constructor(data: any) {
		this.options = data.options;
		this.label = data.label;
	}
}

/** Type class for InputTextarea component */
export class InputTextareaType implements IFormFieldType {
	/** InputTextarea component name */
	public componentName: string = INPUT_TEXTAREA;
	/** Event dictionary with event name as key and callback function as value */
	// tslint:disable-next-line
	public events: {[key: string]: Function} = {};
	/** InputTextarea placeholder prop */
	public placeholder: string;
	/** InputTextarea label prop */
	public label: string;
	/** InputTextarea rows prop */
	public rows: number;
	/** Initializes InputTextarea props */
	public constructor(data: any) {
		this.placeholder = data.placeholder;
		this.label = data.label;
		this.rows = data.rows;
	}
}
