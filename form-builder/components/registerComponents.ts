import Vue from 'vue';
import InputText from './InputText.vue';
import InputRadio from './InputRadio.vue';
import DropDown from './DropDown.vue';
import InputCheckbox from './InputCheckbox.vue';
import InputTextarea from './InputTextarea.vue';
import { INPUT_TEXT, INPUT_RADIO, DROP_DOWN, INPUT_CHECKBOX, INPUT_TEXTAREA } from './shared/constants';

/**
 * Register input components globally in vue
 */
export const register = (): void => {
	Vue.component(INPUT_TEXT, InputText);
	Vue.component(INPUT_RADIO, InputRadio);
	Vue.component(DROP_DOWN, DropDown);
	Vue.component(INPUT_CHECKBOX, InputCheckbox);
	Vue.component(INPUT_TEXTAREA, InputTextarea);
};
