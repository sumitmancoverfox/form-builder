# InputTextarea

Textarea input control.<br/>Extends <b>mixin</b> `ComponentMixin`.

## Props

<!-- @vuese:InputTextarea:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|placeholder|Placeholder for textarea|`String`|`false`|-|
|label|Label displayed for text area control|`String`|`false`|-|
|rows|Number of rows text area should be of|`Number`|`false`|7|

<!-- @vuese:InputTextarea:props:end -->


