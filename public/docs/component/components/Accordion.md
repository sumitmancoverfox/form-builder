# Accordion

Accordion controls a collection of `collapsible` components to have one actively open at a time. It also provides public methods like `open`, `next` and `previous` to manipulate the `collapsibles`.

## Props

<!-- @vuese:Accordion:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|value|`v-model` to store active `collapsible id`.|`String`|`false`|-|
|mode|Defines mode (`AccordionMode`) in which Accordion should operate.|`AccordionMode`|`false`|AccordionMode.EnableAll|
|isNumbered|Generates numbered collapsibles.|`Boolean`|`false`|true|
|numberStartsWith|To start numbering with. Only considered when `isNumbered` is set true.|`Number`|`false`|1|

<!-- @vuese:Accordion:props:end -->


## Events

<!-- @vuese:Accordion:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|input|`v-model` input event.|`:string`<br/>Collapsible id of active collapsible|

<!-- @vuese:Accordion:events:end -->


## Slots

<!-- @vuese:Accordion:slots:start -->
|Name|Description|Default Slot Content|
|---|---|---|
|default|Provide `collapsible`s|-|

<!-- @vuese:Accordion:slots:end -->


## Methods

<!-- @vuese:Accordion:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|next|Activates the `collapsible` which comes after the currently active one in order. <br/><b>Returns</b> collapsible id `:string` of the next `collapsible` (which is being activated). If current `collapsible` is the last one then `null` is returned.|-|
|previous|Activates the `collapsible` which comes before the currently active one in order. <br/><b>Returns</b> collapsible id `:string` of the previous `collapsible` (which is being activated). If current `collapsible` is the first one then `null` is returned.|-|
|open|Activates the `collapsible`, the id of which is passed as an argument.|<b>id</b>`:string` <br/>Collapsible id|

<!-- @vuese:Accordion:methods:end -->


