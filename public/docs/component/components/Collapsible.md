# Collapsible

Collapsible provides a container which can be expanded or collapsed. It also provides public methods like `open`, `close` and `toggle` to manipulate collapsible behaviour.

## Props

<!-- @vuese:Collapsible:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|id|Unique identifier. *Required when using with* `accordion` *component*|`String`|`false`|-|
|openOnInit|Sets initial state of collapsible to expanded if true.|`Boolean`|`false`|false|
|isFilled|Sets `filled` class to collapsible header if true.|`Boolean`|`false`|false|
|openOnly|Collapsible will expand on click but not collapse when clicked again. *Note: This only affects user click. Collapsible can still be expanded or collapsed programatically.*|`Boolean`|`false`|false|
|disabled|Disables collapsible, such that user clicks do not change the state of collapsible. *Note: This only affects user click. Collapsible state can still be changed programatically.*|`Boolean`|`false`|false|
|counter|Sets a number to the collapsible|`Number`|`false`|-|
|beforeOpen|This functions is run before collapsible expands. Return `true` to expand and `false` to keep collapsible in collapsed state.|`Function`|`false`|-|
|beforeClose|This functions is run before collapsible collapses. Return `true` to collapse and `false` to keep collapsible in expanded state.|`Function`|`false`|-|

<!-- @vuese:Collapsible:props:end -->


## Events

<!-- @vuese:Collapsible:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|open|Fired when collapsible expands|`:string`<br/>collapsible id|
|close|Fired when collapsible collapses|`:string`<br/>collapsible id|

<!-- @vuese:Collapsible:events:end -->


## Slots

<!-- @vuese:Collapsible:slots:start -->
|Name|Description|Default Slot Content|
|---|---|---|
|counter|Display collapsible `counter` number. `counter` available in slot properties|`counter` property of collapsible|
|header|Collapsible header|-|
|default|Content body of collapsible|-|

<!-- @vuese:Collapsible:slots:end -->


## Methods

<!-- @vuese:Collapsible:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|isCollapsibleOpen|`getter`<br/><b>Returns</b> true `:boolean` if collapsible is expanded|-|
|isCollapsibleClosed|`getter`<br/><b>Returns</b> true `:boolean` if collapsible is collapsed|-|
|collapsibleId|`getter`<br/><b>Returns</b> collapsible id `:string`|-|
|isCollapsibleFilled|`getter`<br/><b>Returns</b> true `:boolean` if collapsible is filled|-|
|open|Expands `collapsible` and then emits `open` event|<b>forceOpen</b>`:boolean` <br/>Pass `true` to surpass checks (`beforeOpen`) and expand the collapsible.|
|close|Collapses `collapsible` and then emits `close` event|<b>forceClose</b>`:boolean` <br/>Pass `true` to surpass checks (`beforeClose`) and collapse the collapsible.|
|toggle|Toggles the state of collapsible between `open` and `close`|-|

<!-- @vuese:Collapsible:methods:end -->


