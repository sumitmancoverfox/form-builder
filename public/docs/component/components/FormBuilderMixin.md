# FormBuilderMixin

Mixin to provide and export common form functionalities.

## Props

<!-- @vuese:FormBuilderMixin:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|id|Unique identifier for the form builder|`String`|`true`|-|
|forms|Array of forms `:IForm` and properties to be rendered|`Array`|`true`|[]|
|data|Dictionary of field id and data tslint:disable:arrow-return-shorthand|`Object`|`false`|-|
|value|`v-model` to store active form id|`String`|`false`|-|
|typeMap|Dictionary of input type names `:string` and classes `:IFormFieldTypeConstructor` to represent input components. <br/><i><small>Note: Classes must implement `:IFormFieldType` and have props and events required by the input component they represent.</small></i>|`Object`|`false`|-|
|showUpdateButton|Displays an "Update and review" button on steps (except the last step), once all the forms are submitted.|`Boolean`|`false`|true|

<!-- @vuese:FormBuilderMixin:props:end -->


## Events

<!-- @vuese:FormBuilderMixin:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|submit|Fired when form submit button is clicked|-|
|update|Fired when form "Update and review" button is clicked.|`:IForm`<br/>Currently active form|
|input|`v-model` input event.|`:string`<br/>Form id of currently active form|
|formInitialized|Triggers after every time the form and its objects are initialized|-|

<!-- @vuese:FormBuilderMixin:events:end -->


## Methods

<!-- @vuese:FormBuilderMixin:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|getFieldRef|Fetches the reference element of form field. <br/><b>Returns</b> reference element or `null` if fieldId does not exist.|<b>fieldId</b>`:string`<br/>Field id specified in the form json.|
|activeFormIndex|`getter`<br/><b>Returns</b> index `:number` of the currently active form|-|
|activeForm|`getter`<br/><b>Returns</b> currently active form object `:IForm`|-|
|firstVisibleFormIndex|`getter`<br/><b>Returns</b> index of first visible form `:number`|-|
|lastVisibleFormIndex|`getter`<br/><b>Returns</b> index of last visible form `:number`|-|
|previousFormIndex|Accepts form index `:number` and <br/><b>Returns</b> index of previous visible form `:number`|-|
|nextFormIndex|Accepts form index `:number` and <br/><b>Returns</b> index of next visible form `:number`|-|

<!-- @vuese:FormBuilderMixin:methods:end -->


