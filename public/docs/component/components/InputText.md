# InputText

Text input control.<br/>Extends <b>mixin</b> `ComponentMixin`.

## Props

<!-- @vuese:InputText:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|placeholder|Placeholder for input text|`String`|`false`|-|
|label|Label displayed for input text control|`String`|`false`|-|

<!-- @vuese:InputText:props:end -->


