# FormStepCounter

## Props

<!-- @vuese:FormStepCounter:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|forms|Array of forms `:IForm`|`Array`|`true`|-|
|activeFormId|Form id of active form|`String`|`false`|-|

<!-- @vuese:FormStepCounter:props:end -->


## Events

<!-- @vuese:FormStepCounter:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|stepClick|Fired when a step is clicked|`:IStepClickEvent`<br/>Form properties of the form clicked on|

<!-- @vuese:FormStepCounter:events:end -->


## Slots

<!-- @vuese:FormStepCounter:slots:start -->
|Name|Description|Default Slot Content|
|---|---|---|
|stepNumber|Display step number. index `:number`, form `:IForm`, canNavigate `:boolean` available in slot properties.|Form index + 1|
|stepLabel|Display step label. index `:number`, form `:IForm`, canNavigate `:boolean` available in slot properties.|Form name|

<!-- @vuese:FormStepCounter:slots:end -->


