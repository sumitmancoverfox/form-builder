# FormRenderer

Renders the form with it's fields

## Props

<!-- @vuese:FormRenderer:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|data|Dictionary of field id and data tslint:disable:arrow-return-shorthand|`Object`|`false`|-|
|activeForm|Form object `:IForm` to be rendered|`Object`|`true`|-|
|typeMap|Dictionary of input type names `:string` and classes `:IFormFieldTypeConstructor` to represent input components. <br/><i><small>Note: Classes must implement `:IFormFieldType` and have props and events required by the input component they represent.</small></i>|`Object`|`false`|-|
|showUpdateButton|Displays an "Update and review" button on steps (except the last step), once all the forms are submitted.|`Boolean`|`false`|-|
|isUpdateButtonVisible|Hides/Shows "Update and review" button. Works only when used with `showUpdateButton` set to true.|`Boolean`|`false`|-|

<!-- @vuese:FormRenderer:props:end -->


## Events

<!-- @vuese:FormRenderer:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|update|Fired when form "Update and review" button is clicked.|`:IForm`<br/>Currently active form|
|submit|Fired when form submit button is clicked|-|

<!-- @vuese:FormRenderer:events:end -->


## Slots

<!-- @vuese:FormRenderer:slots:start -->
|Name|Description|Default Slot Content|
|---|---|---|
|formButtons|Replace default submit and update buttons with your own implementation. Form object activeForm `:IForm` and showUpdateAndSkip `:boolean` available in slot properties.<br/><i>Note: showUpdateAndSkip will be true when all forms are submitted and active form is not the last form</i>|-|
|updateButtonContent|Display text of "Update and review" button. Form object activeForm `:IForm` available in slot properties|`Update & Review`|
|submitButtonContent|Display text of submit button. Form object activeForm `:IForm` available in slot properties|`Save & Continue`|

<!-- @vuese:FormRenderer:slots:end -->


## Methods

<!-- @vuese:FormRenderer:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|getFieldRef|Fetches the reference element of form field. <br/><b>Returns</b> reference element or `null` if fieldId does not exist.|<b>fieldId</b>`:string`<br/>Field id specified in the form json.|

<!-- @vuese:FormRenderer:methods:end -->


