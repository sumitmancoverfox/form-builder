# FormReview

Renders a review page w.r.t. forms `:IForm` array

## Props

<!-- @vuese:FormReview:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|id|Unique identifier for the form builder|`String`|`true`|-|
|data|Dictionary of field id and data tslint:disable:arrow-return-shorthand|`Object`|`false`|-|
|forms|Array of forms `:IForm` and properties to be rendered|`Array`|`true`|-|

<!-- @vuese:FormReview:props:end -->


## Events

<!-- @vuese:FormReview:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|formEdit|Fired when edit on a section is clicked.|`:IForm`<br/> Form details|

<!-- @vuese:FormReview:events:end -->


## Slots

<!-- @vuese:FormReview:slots:start -->
|Name|Description|Default Slot Content|
|---|---|---|
|form.formId + '/' + section.sectionId|Display custom implementation of a form section review. section `:IFormSection`, form `:IForm`, forms `:IForm[]` available in slot properties.|Default section implementation|
|form.formId+'/'+section.sectionId+'/'+field.fieldId|Display custom implementation of a form field review. field `:IFormField`, section `:IFormSection`, form `:IForm`, forms `:IForm[]` available in slot properties.|Default field implementation|

<!-- @vuese:FormReview:slots:end -->


