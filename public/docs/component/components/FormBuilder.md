# FormBuilder

Encapsulates the `FormRenderer` and `FormStepCounter`, and exposes their <b>slots</b>. <br/>Extends <b>mixin</b> `FormBuilderMixin`.

## Props

<!-- @vuese:FormBuilder:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|showNavigation|Shows previous navigation button on the form, if true.|`Boolean`|`false`|false|

<!-- @vuese:FormBuilder:props:end -->


## Events

<!-- @vuese:FormBuilder:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|previousFormClick|-|-|
|stepClick|Fired when a step on the `FormStepCounter` is clicked|`:IStepClickEvent`<br/>Form properties of the form clicked on|

<!-- @vuese:FormBuilder:events:end -->


## Slots

<!-- @vuese:FormBuilder:slots:start -->
|Name|Description|Default Slot Content|
|---|---|---|
|updateButtonContent|Display text of "Update and review" button. Form object activeForm `:IForm` available in slot properties|-|
|submitButtonContent|Display text of submit button. Form object activeForm `:IForm` available in slot properties|-|

<!-- @vuese:FormBuilder:slots:end -->


