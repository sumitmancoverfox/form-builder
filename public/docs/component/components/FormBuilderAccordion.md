# FormBuilderAccordion

Encapsulates `FormRenderer` and generates forms inside `Accordion` and `Collapsibles`. It also exposes <b>slots</b> provided by `FormRenderer` and `FormStepCounter`. <br/>Extends <b>mixin</b> `FormBuilderMixin`.

## Props

<!-- @vuese:FormBuilderAccordion:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|beforeOpen|`beforeOpen` function used by `Collapsible`|`Function`|`false`|-|
|beforeClose|`beforeClose` function used by `Collapsible`|`Function`|`false`|-|

<!-- @vuese:FormBuilderAccordion:props:end -->


## Events

<!-- @vuese:FormBuilderAccordion:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|stepClick|Fired when a `Collapsible` is clicked|`:IStepClickEvent`<br/>Form properties of the form being clicked|

<!-- @vuese:FormBuilderAccordion:events:end -->


## Slots

<!-- @vuese:FormBuilderAccordion:slots:start -->
|Name|Description|Default Slot Content|
|---|---|---|
|stepLabel|Display step label. index `:number`, form `:IForm`, canNavigate `:boolean` available in slot properties.|-|
|updateButtonContent|Display text of "Update and review" button. Form object activeForm `:IForm` available in slot properties|-|
|submitButtonContent|Display text of submit button. Form object activeForm `:IForm` available in slot properties|-|

<!-- @vuese:FormBuilderAccordion:slots:end -->


## Methods

<!-- @vuese:FormBuilderAccordion:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|getFieldRef|Fetches the reference element of form field. <i>overrides `getFieldRef` from mixin.</i> <br/><b>Returns</b> reference element or `null` if fieldId does not exist.|<b>fieldId</b>`:string`<br/>Field id specified in the form json.|

<!-- @vuese:FormBuilderAccordion:methods:end -->


